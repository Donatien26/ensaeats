from app.api.app import app
import uvicorn

if __name__ == '__main__':
    uvicorn.run(app, port=int(8080), host="localhost")
