FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8
COPY ./requirements.txt /app/requirements.txt
COPY ./app /app/app
RUN cd /app && pip install -v -r requirements.txt

