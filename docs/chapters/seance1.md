﻿# Séance 1

<!-- .slide: class="slide" -->

---

### Objectif de la séance

<!-- .slide: class="slide" -->

- Pas de code !!!
- S'approprier le sujet
- Séance dédié à la modélisation + réalisation de diagramme
- Travail en "autonomie"
- Produire le rapport intermédiaire

Objectif : Me faire comprendre que vous avez compris le sujet

---

### Contenu du rapport intermédiaire

<!-- .slide: class="slide" -->

- Diagramme de Gant (Plannification)
- Diagramme de cas d'utilisation
- Diagramme d'activité
- Diagramme de base de donnée
- 1 ou 2 diagrammes de séquences
- Diagramme de packages
- Diagramme d'architecture
- Des commentaires de vos diagrammes

---

### Petits conseils

<!-- .slide: class="slide" -->

- N'hésitez pas à venir me voir
- Définissez un chef de projet
- Définissez un chef technique != chef projet
- N'attendez pas le dernier moment pour vous y mettre, le dossier intermédiaire est qqch de trés important !!! (1/3 note finale)
