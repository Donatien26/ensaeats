### Objectif du projet

<!-- .slide: class="slide" -->

- Produire une application
- ⚠️ Pas d'interface graphique
- Privilégier l'ajout de fonctionnalitées

---

### Présentation

<!-- .slide: class="slide" -->

- Utiliser l'api https://api.yelp.com/v3 afin de produire une application de type UberEats (Passer des commandes auprès de restaurateurs)

---

### Fonctionnalités minimale

<!-- .slide: class="slide" -->

- Effectuer des appels à l’API afin de récupérer des restaurants en fonction de theme, ou de la localisation.
- Pouvoir ajouter des produits pour un restaurants (menu, plats, boissons,...).
- Consulter les produits d'un établissement
- Enregistrer des commandes
- Consulter la liste des commandes pour un restaurant / un utilisateur
- Avoir une api fonctionnelle
- Avoir un client qui utilise votre api pour un utilisateur

---

### Fonctionnalités avancées

<!-- .slide: class="slide" -->

- Une interface console simple à destination des restaurateurs qui utilisera votre API (cf architecture client serveur)
- Une gestion de l'état des commandes
- Une gestion de l'authentification un utilisateur ne peut pas mettre a jour un menu etc ..
- Toute autre idée qui vous fera plaisir

---

### Architecture Client/Serveur

---

### C'est quoi une API

---

### Yelp

- Une api libre d'accès (dans la limite de 5000 appel/jour) mais sous authentification / token
- Documentation ici: https://www.yelp.com/developers/documentation/v3

---

### Git

---

### Vscode

---

### FastApi

---

### SSPcloud

---

### gitlab-ci

---

### Docker

---

### Kubernetes
