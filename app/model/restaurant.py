
from app.model.review import Review
from app.model.location import Location
from app.model.items import Items
from typing import List


class Restaurant():
    def __init__(self, name: str, address: Location, reviews: List[Review], globalPrice: str, sellList: List[Items]) -> None:
        self.name = name
        self.address = address
        self.reviews = reviews
        self.globalPrice = globalPrice
        self.sellList = sellList
