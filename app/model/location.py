class Location:

    def __init__(self, line1: str, line2: str, line3: str, city: str, state: str, zip_code: str, country: str) -> None:
        self.line1 = line1
        self.line2 = line2
        self.line3 = line3
        self.city = city
        self.state = state
        self.zip_code = zip_code
        self.country = country
