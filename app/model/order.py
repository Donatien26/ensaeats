from app.model.order_status import OrderStatus
from datetime import datetime
from typing import List
from app.model.items import Items


class Order():

    def __init__(self, date: datetime, items: List[Items], status: OrderStatus) -> None:
        self.date = date
        self.items = items
        self.status = status

    def getCostOfOrder(self):
        sum(i.price for i in self.items)
