from enum import Enum


class OrderStatus(Enum):

    IN_KITCHEN = "in kitchen"
    DELIVERED = "delivered"
