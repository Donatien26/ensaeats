from app.model.items import Items
from typing import List
from app.model.dish import Dish


class Menu(Items):

    def __init__(self, name: str, description: str, price: float, entrees: List[Dish], dishes: List[Dish], desserts: List[Dish]) -> None:
        super().__init__(name, description, price)
        self.entrees = entrees
        self.dishes = dishes
        self.desserts = desserts
