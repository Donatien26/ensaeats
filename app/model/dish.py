
from app.model.items import Items


class Dish(Items):

    def __init__(self, name, description, price) -> None:
        super().__init__(name, description, price)
