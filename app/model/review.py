class Review:

    def __init__(self, rating: int, text: str, time: str, user: str) -> None:
        self.rating = rating
        self.text = text
        self.time = time
        self.user = user
