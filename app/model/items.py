from abc import ABC


class Items(ABC):

    def __init__(self, name, description, price) -> None:
        self.name = name
        self.description = description
        self.price = price
