from fastapi import FastAPI
from app.api.controller import buisness_controller
from app.api.configuration.sql_alchemy_configuration import Base, engine
app = FastAPI(title="EnsaEats",
              description="Commandez vos repas dès maintenant",
              version="2.5.0",
              )

app.include_router(buisness_controller.router)
Base.metadata.create_all(engine)
