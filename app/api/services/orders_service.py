class OrdersService:

    @staticmethod
    def getOrdersForRestaurant(id_restaurant):
        raise NotImplementedError

    @staticmethod
    def getOrderForRestaurant(id_restaurant, id_order):
        raise NotImplementedError

    @staticmethod
    def updateStatusOrderForRestaurant(id_restaurant, id_order, status):
        raise NotImplementedError

    @staticmethod
    def addOrderForRestaurant(id_restaurant, order):
        raise NotImplementedError
