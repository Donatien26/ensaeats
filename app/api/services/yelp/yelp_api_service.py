import requests
from dotenv import dotenv_values
from requests.models import Response
from app.api.services.yelp.yelp_api_mapper import YelpMapper


class YelpApiService:

    yelp_token = dotenv_values(".env.YELP_TOKEN")

    @staticmethod
    def getBuisness(id: str) -> Response:
        url = "https://api.yelp.com/v3/businesses/{}".format(id)
        response = requests.get(
            url, headers={'Authorization': "bearer "+YelpApiService.yelp_token})
        return response

    @staticmethod
    def getBuisnesses(term: str, location: str, radius: str) -> Response:
        url = "https://api.yelp.com/v3/businesses/search"
        response = requests.get(
            url, params={"term": term, "location": location, "radius": int(radius)}, headers={'Authorization': "bearer "+YelpApiService.yelp_token})
        return response
