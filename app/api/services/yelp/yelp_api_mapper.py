from app.model.location import Location
from app.model.review import Review
from app.model.restaurant import Restaurant


class YelpMapper:

    @staticmethod
    def buisness_to_restaurant(response) -> Restaurant:
        raise NotImplementedError

    @staticmethod
    def yelp_review_to_review(response) -> Review:
        raise NotImplementedError

    @staticmethod
    def yelp_location_to_location(response) -> Location:
        raise NotImplementedError
