from sqlalchemy import Column, Integer, String
from app.db.configuration.sql_alchemy_configuration import Base


class Items(Base):

    __abstract__ = True

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    prix = Column(String)
