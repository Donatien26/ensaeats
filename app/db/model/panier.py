from sqlalchemy.orm import relationship
from app.db.model.plat import Plat
from app.db.configuration.sql_alchemy_configuration import Base
from sqlalchemy import Column, Integer, ForeignKey


class Panier(Base):

    __tablename__ = "Panier"
    id = Column(Integer, primary_key=True)
    elements = relationship(Plat)
    billing_address_id = Column(Integer, ForeignKey("address.id"))

    def add_element_to_panier(self, items: Plat) -> None:
        self.elements.append(items)
