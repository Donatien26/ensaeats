from app.model.items import Items
from app.api.configuration.sql_alchemy_configuration import Base
from sqlalchemy import Column, Integer, String


class Plat(Items):

    __tablename__ = 'Plats'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    prix = Column(String)
