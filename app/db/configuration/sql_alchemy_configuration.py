from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session
engine = create_engine('sqlite://', echo=True)
Base = declarative_base()
session: Session = sessionmaker(bind=engine)()
