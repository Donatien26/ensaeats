from app.model.plat import Plat
from app.model.items import Items
from app.model.panier import Panier
from app.api.configuration.sql_alchemy_configuration import session, Base, engine

Base.metadata.create_all(engine)

plat = Items(id=1, name="toto", description="toto", prix="25.8")
panier = Panier(id=1, elements=[])
session.add(panier)
session.commit()

query = session.query(Panier).all()
instance = query.first()
print(panier.elements)
