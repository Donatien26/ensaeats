
EnsaEats
Tuteur : Donatien ENEMAN

# Présentation
Cela fait bientot 2 ans que l'EJR, comme beaucoup d'enseignes de restauration, a dû fermer ses portes à cause de la situation sanitaire. Les caisses sont au plus mals, il est grand temps pour l'EJR de se renouveller. Leur but inspirer de uberEats : se faire de l'argent sans préparer à manger. Votre mission, pour leur venir en aide, c'est de développer une API qui permettra de mettre en relation des restaurateurs et leurs clients. Cette API devra permettre au client de chercher les restaurateurs dans un lieu donné, de consulter les détails sur les restaurants (et leur menu), et de passer des commandes. Cette API devra également permettre aux restaurateurs d'ajouter, modfier, supprimer leur plat/menu. Pour avoir accès à une base de données des restaurants nous utiliseront l'API de Yelp. 

# Fonctionnalités de base
- Une API fonctionnelle : rechercher des restaurants (possibilité de filtrer), consulter les détails d'un restaurant, passer une commande, ajouter/modifier/supprimer des plats/menus pour chaque restaurant, consulter les commandes à destination d'un restaurant
- Des tests unitaires
- Une interface console simple à destination des clients qui utilisera votre API (cf architecture client serveur)


# Fonctionnalités avancées
- Une interface console simple à destination des restaurateurs  qui utilisera votre API (cf architecture client serveur)
- Une gestion de l'authentification



# Les outils
- Framework fastApi
- Base Postgres
- Api Yelp
- git et son univers
- Vscode

# Ce qu'on essaiera de voir en plus
Pour les groupes les plus avancés/motivés nous essaierons de voir :
- comment automatiser l'exécution des tests unitaires sur vos codes
- comment déployer en continu votre application sur le web
